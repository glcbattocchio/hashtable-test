package HashTableTEST;

import java.util.Objects;


public class CentroVaccinale {
    String nome;
    int capienza;
    String luogo;
    
    public CentroVaccinale(String nome, String luogo, int capienza)
    {
        this.nome = nome;
        this.capienza = capienza;
        this.luogo = luogo;
    }

    @Override
    public String toString() {
        return nome+"|"+capienza+"|"+luogo;
    }


    

    public boolean equals(Object o) {
      if (o == this) 
          return true;
      if (!(o instanceof CentroVaccinale))
        return false;
      

      CentroVaccinale cv = (CentroVaccinale) o;
      return (Objects.equals(nome , cv.nome) && Objects.equals(luogo , cv.luogo) && capienza == cv.capienza);
    }

    public int hashCode() {
      return Objects.hash(nome, luogo, capienza);
    }




    
    
    
}
